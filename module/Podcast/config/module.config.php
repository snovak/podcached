<?

  return array(
      'controllers' => array(
          'invokables' => array(
              'Podcast\Controller\Podcast' => 'Podcast\Controller\PodcastController',
          ),
      ),

      'router' => array(
          'routes' => array(
              'podcast' => array(
                  'type'    => 'segment',
                  'options' => array(
                      'route'    => '/podcast[/:action][/:id]',
                      'constraints' => array(
                          'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                          'id'     => '[0-9]+',
                      ),
                      'defaults' => array(
                          'controller' => 'Podcast\Controller\Podcast',
                          'action'     => 'index',
                      ),
                  ),
              ),
          ),
      ),

      'view_manager' => array(
          'template_path_stack' => array(
              'podcast' => __DIR__ . '/../view',
          ),
      ),
  );

?>
