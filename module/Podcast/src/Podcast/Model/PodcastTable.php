<?
namespace Podcast\Model;

use Zend\Db\TableGateway\TableGateway;

class PodcastTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getPodcast($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function savePodcast(Podcast $podcast)
    {
        $data = array(
            'title' => $podcast->title,
            'content'  => $podcast->content,
        );

        $id = (int) $podcast->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getPodcast($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Podcast id does not exist');
            }
        }
    }

    public function deletePodcast($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}


?>
