<?
namespace Podcast\Controller;

 use Zend\Mvc\Controller\AbstractActionController;
 use Zend\View\Model\ViewModel;

 use Podcast\Model\Podcast;
 use Podcast\Form\PodcastForm;

 class PodcastController extends AbstractActionController
 {
    protected $podcastTable;

    public function indexAction()
    {
      return new ViewModel(array(
        'podcasts' => $this->getPodcastTable()->fetchAll(),
      ));
    }

    public function addAction()
    {
      $form = new PodcastForm();
      $form->get('submit')->setValue('Add');

      $request = $this->getRequest();
      if ($request->isPost()) {
          $podcast = new Podcast();
          $form->setInputFilter($podcast->getInputFilter());
          $form->setData($request->getPost());

          if ($form->isValid()) {
              $podcast->exchangeArray($form->getData());
              $this->getPodcastTable()->savePodcast($podcast);

              // Redirect to list of podcasts
              return $this->redirect()->toRoute('podcast');
          }
      }
      return array('form' => $form);
    }//PodcastController


    public function editAction()
    {
      $id = (int) $this->params()->fromRoute('id', 0);
      if (!$id) {
          return $this->redirect()->toRoute('podcast', array(
              'action' => 'add'
          ));
      }

      // Get the Podcast with the specified id.  An exception is thrown
      // if it cannot be found, in which case go to the index page.
      try {
          $podcast = $this->getPodcastTable()->getPodcast($id);
      }
      catch (\Exception $ex) {
          return $this->redirect()->toRoute('podcast', array(
              'action' => 'index'
          ));
      }

      $form  = new PodcastForm();
      $form->bind($podcast);  //attaches the Model to the Form
      $form->get('submit')->setAttribute('value', 'Edit');

      $request = $this->getRequest();
      if ($request->isPost()) {
          $form->setInputFilter($podcast->getInputFilter());
          $form->setData($request->getPost());

          if ($form->isValid()) {
              $this->getAlbumTable()->saveAlbum($podcast);

              // Redirect to list of albums
              return $this->redirect()->toRoute('podcast');
          }
      }

      return array(
          'id' => $id,
          'form' => $form,
      );
    }  //editAction

    public function deleteAction()
    {
      $id = (int) $this->params()->fromRoute('id', 0);
      if (!$id) {
          return $this->redirect()->toRoute('podcast');
      }

      $request = $this->getRequest();
      if ($request->isPost()) {
          $del = $request->getPost('del', 'No');

          if ($del == 'Yes') {
              $id = (int) $request->getPost('id');
              $this->getPodcastTable()->deletePodcast($id);
          }

          // Redirect to list of albums
          return $this->redirect()->toRoute('podcast');
      }

      return array(
          'id'    => $id,
          'podcast' => $this->getPodcastTable()->getPodcast($id)
      );
    }

    public function getPodcastTable()
    {
    if (!$this->podcastTable) {
      $sm = $this->getServiceLocator();
      $this->podcastTable = $sm->get('Podcast\Model\PodcastTable');
    }
    return $this->podcastTable;
  }//getPodcastTable
 }
 ?>
